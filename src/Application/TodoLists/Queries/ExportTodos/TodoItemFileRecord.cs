﻿using ClearArchitecture.Application.Common.Mappings;
using ClearArchitecture.Domain.Entities;

namespace ClearArchitecture.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }
        public bool Done { get; set; }
    }
}
