﻿using ClearArchitecture.Domain.Common;
using System.Threading.Tasks;

namespace ClearArchitecture.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
