﻿using ClearArchitecture.Domain.Common;
using ClearArchitecture.Domain.Entities;

namespace ClearArchitecture.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
