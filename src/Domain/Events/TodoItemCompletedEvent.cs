﻿using ClearArchitecture.Domain.Common;
using ClearArchitecture.Domain.Entities;

namespace ClearArchitecture.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
