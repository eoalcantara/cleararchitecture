﻿using ClearArchitecture.Application.Common.Interfaces;
using System;

namespace ClearArchitecture.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
