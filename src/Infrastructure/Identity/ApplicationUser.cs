﻿using Microsoft.AspNetCore.Identity;

namespace ClearArchitecture.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
