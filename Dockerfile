
# Baixa a versão do dotnet 5.0 SDK

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y nodejs

#Setando uma pasta para colocar os projetos
WORKDIR /src

COPY ["/src/WebUI/WebUI.csproj", "WebUI/"]
COPY ["/src/Application/Application.csproj", "Application/"]
COPY ["/src/Domain/Domain.csproj", "Domain/"]
COPY ["/src/Infrastructure/Infrastructure.csproj", "Infrastructure/"]

RUN dotnet clean "WebUI/WebUI.csproj"
RUN dotnet restore "WebUI/WebUI.csproj"

#WORKDIR "/WebUI"

RUN dotnet build "WebUI/WebUI.csproj" -c Release -o out

FROM build AS publish
RUN dotnet publish "WebUI/WebUI.csproj" -c Release -o out

FROM base AS final
#WORKDIR /src
COPY --from=publish /out/ .

#ENTRYPOINT ["dotnet", "ClearArchitecture.WebUI.dll"]

CMD ASPNETCORE_URLS=http://*:$PORT dotnet ClearArchitecture.WebUI.dll
